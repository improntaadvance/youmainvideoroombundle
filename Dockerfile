FROM php:8.1.4-cli-alpine

RUN apk add --no-cache zip libzip-dev \
    && docker-php-ext-install zip

# # Update apt repositories
# RUN apt-get update

# # Install libzip
# RUN apt-get install -y git zip unzip libzip-dev \
#     && docker-php-ext-configure zip \
#     && docker-php-ext-install zip

# # Install db extensions
# RUN docker-php-ext-install pdo pdo_mysql

# # Install op cache
# RUN docker-php-ext-install opcache \
#     && docker-php-ext-enable opcache

# # Install i18n extensions
# RUN apt-get install -y libicu-dev locales \
#     && docker-php-ext-install intl \
#     && docker-php-ext-enable intl \
#     && echo "it_IT.UTF-8 UTF-8" > /etc/locale.gen \
#     && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
#     && locale-gen


# # Set localtime to Europe/Rome
# RUN ln -snf /usr/share/zoneinfo/Europe/Rome /etc/localtime \
#     && echo 'Europe/Rome' > /etc/timezone

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# # Install my config
# RUN apt-get install -y vim

# RUN echo 'alias ll="ls -lah"' >> ~/.bashrc

WORKDIR /app