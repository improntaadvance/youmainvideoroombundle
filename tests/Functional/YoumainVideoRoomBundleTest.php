<?php

namespace Tests\Youmain\VideoRoomBundle\Functional;

use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Messenger\MessageBusInterface;
use Youmain\VideoRoomBundle\Service\TwilioRoomManager;
use Youmain\VideoRoomBundle\Service\TwilioWebhookHandler;
use Youmain\VideoRoomBundle\YoumainVideoRoomBundle;

class YoumainVideoRoomBundleTest extends TestCase
{
    public function test_basic_configuration(): void
    {
        $kernel = new YoumainVideoRoomBundleTestingKernel([
            'twilio_sid' => 'foo',
            'twilio_token' => 'foo',
            'twilio_apikey_sid' => 'foo',
            'twilio_apikey_secret' => 'foo',
            // 'twilio_webhook' => '',
            // 'twilio_webhook_logger' => '',
        ]);
        $kernel->boot();
        $container = $kernel->getContainer();

        $roomManager = $container->get('youmain.twilio_room_manager');

        $this->assertInstanceOf(TwilioRoomManager::class, $roomManager);
    }

    public function test_webhook_configuration(): void
    {
        $kernel = new YoumainVideoRoomBundleTestingKernel([
            'twilio_sid' => 'foo',
            'twilio_token' => 'foo',
            'twilio_apikey_sid' => 'foo',
            'twilio_apikey_secret' => 'foo',
            'twilio_webhook' => 'http://localhost',
            // 'twilio_webhook_logger' => '',
        ]);
        $kernel->boot();
        $container = $kernel->getContainer();

        $roomManager = $container->get('youmain.twilio_room_manager');

        $this->assertInstanceOf(TwilioRoomManager::class, $roomManager);
    }

    public function test_webhook_logger_configuration(): void
    {
        $kernel = new YoumainVideoRoomBundleTestingKernel([
            'twilio_sid' => 'foo',
            'twilio_token' => 'foo',
            'twilio_apikey_sid' => 'foo',
            'twilio_apikey_secret' => 'foo',
            'twilio_webhook' => 'http://localhost',
            'twilio_webhook_logger' => 'monolog.logger.twilio',
        ], [
            'monolog.logger.twilio' => \Psr\Log\LoggerInterface::class,
        ]);
        $kernel->boot();
        $container = $kernel->getContainer();
        $container->set('monolog.logger.twilio', $this->createMock(\Psr\Log\LoggerInterface::class));

        /**
         * @var TwilioWebhookHandler
         */
        $handler = $container->get('youmain.twilio_webhook_handler');

        $this->assertInstanceOf(TwilioWebhookHandler::class, $handler);

        $this->expectException(\RuntimeException::class);

        $handler->handle(new Request());
    }

    public function test_webhook_event_bus_configuration(): void
    {
        $kernel = new YoumainVideoRoomBundleTestingKernel([
            'twilio_sid' => 'foo',
            'twilio_token' => 'foo',
            'twilio_apikey_sid' => 'foo',
            'twilio_apikey_secret' => 'foo',
            'twilio_webhook' => 'http://localhost',
            'twilio_webhook_event_bus' => 'my.event.bus',
        ], [
            'my.event.bus' => MessageBusInterface::class,
        ]);
        $kernel->boot();
        $container = $kernel->getContainer();
        $container->set('my.event.bus', $this->createMock(MessageBusInterface::class));

        /**
         * @var TwilioWebhookHandler
         */
        $handler = $container->get('youmain.twilio_webhook_handler');

        $this->assertInstanceOf(TwilioWebhookHandler::class, $handler);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Exception in parseEvent');

        $handler->handle(new Request());
    }
}

class YoumainVideoRoomBundleTestingKernel extends Kernel
{
    /**
     * @var array<string, string>
     */
    private array $config;

    /**
     * @var array<string, string>
     */
    private array $services;

    /**
     * @param array<string, string> $config
     * @param array<string, string> $services
     */
    public function __construct(array $config = [], array $services = [])
    {
        $this->config = $config;
        $this->services = $services;
        parent::__construct('test', true);
    }

    public function registerBundles(): iterable
    {
        return [
            new YoumainVideoRoomBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(function (ContainerBuilder $container) {
            foreach ($this->services as $id => $class) {
                $definition = new Definition($class);
                $definition->setSynthetic(true);
                $container->setDefinition($id, $definition);
            }

            $container->loadFromExtension('youmain_video_room', $this->config);
        });
    }

    public function getCacheDir(): string
    {
        return __DIR__.'/../../var/cache/'.spl_object_hash($this);
    }
}
