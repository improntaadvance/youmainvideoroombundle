<?php

namespace Tests\Youmain\VideoRoomBundle\Unit\Service;

use PHPUnit\Framework\TestCase;
use Youmain\VideoRoomBundle\Model\VideoRoomInterface;
use Youmain\VideoRoomBundle\Service\VideoRoomNamer;

class VideoRoomNamerTest extends TestCase
{
    public function testGenerate(): void
    {
        $videoRoom = $this->createStub(VideoRoomInterface::class);
        $videoRoom->method('getRoomSlug')
            ->willReturn('foo');

        $service = new VideoRoomNamer();

        $this->assertEquals('foo', $service->generate($videoRoom));
    }

    public function testParse(): void
    {
        $service = new VideoRoomNamer();

        $this->assertEquals('foo', $service->parse('foo'));
    }
}
