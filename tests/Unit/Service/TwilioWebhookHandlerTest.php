<?php

namespace Tests\Youmain\VideoRoomBundle\Unit\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Youmain\VideoRoomBundle\Events\RoomCreated;
use Youmain\VideoRoomBundle\Events\RoomEnded;
use Youmain\VideoRoomBundle\Events\RoomParticipantConnected;
use Youmain\VideoRoomBundle\Events\RoomParticipantDisconnected;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackAdded;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackDisabled;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackEnabled;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackRemoved;
use Youmain\VideoRoomBundle\Service\TwilioWebhookHandler;
use Youmain\VideoRoomBundle\Service\VideoRoomNamerInterface;

class TwilioWebhookHandlerTest extends TestCase
{
    public function test_exception_no_dispatcher_available(): void
    {
        $this->expectException(RuntimeException::class);

        /**
         * @var VideoRoomNamerInterface&MockObject
         */
        $videoRoomNamer = $this->createMock(VideoRoomNamerInterface::class);

        $handler = new TwilioWebhookHandler($videoRoomNamer);

        $request = $this->createRequest([
            'StatusCallbackEvent' => 'room-created',
            'RoomName' => 'video-room',
        ]);

        $handler->handle($request);
    }

    /**
     * @dataProvider twilioEvents
     *
     * @param class-string<object> $eventClass
     */
    public function test_basic_handler(string $eventName, string $roomName, string $participant, string $eventClass): void
    {
        /**
         * @var VideoRoomNamerInterface&MockObject
         */
        $videoRoomNamer = $this->createMock(VideoRoomNamerInterface::class);
        $videoRoomNamer
            ->expects($this->once())
            ->method('parse')
            ->with($roomName)
            ->willReturn($roomName);

        /**
         * @var MessageBusInterface&MockObject
         */
        $eventBus = $this->createMock(MessageBusInterface::class);
        $eventBus
            ->expects($this->once())
            ->method('dispatch')
            ->with(self::callback(function ($event) use ($eventClass, $roomName, $participant) {
                $this->assertInstanceOf($eventClass, $event);
                $this->assertEquals($roomName, $event->getRoomId());

                if ('' !== $participant) {
                    $this->assertEquals($participant, $event->getParticipantId());
                }

                return true;
            }))
            ->willReturn(new Envelope($this->createMock($eventClass)));

        /**
         * @var LoggerInterface&MockObject
         */
        $logger = $this->createMock(LoggerInterface::class);
        $logger
            ->expects($this->once())
            ->method('info');

        $handler = new TwilioWebhookHandler($videoRoomNamer, $eventBus, $logger);

        $body = [
            'StatusCallbackEvent' => $eventName,
            'RoomName' => $roomName,
        ];

        if ('' !== $participant) {
            $body['ParticipantIdentity'] = $participant;
        }

        $request = $this->createRequest($body);

        $handler->handle($request);
    }

    /**
     * @return array<string, string[]>
     */
    public function twilioEvents(): array
    {
        return [
            'RoomCreated' => ['room-created', 'video-room-01', '', RoomCreated::class],
            'RoomEnded' => ['room-ended', 'video-room-02', '', RoomEnded::class],
            'ParticipantConnected' => ['participant-connected', 'video-room-02', 'p01', RoomParticipantConnected::class],
            'ParticipantDisconnected' => ['participant-disconnected', 'video-room-03', 'p02', RoomParticipantDisconnected::class],
            'TrackAdded' => ['track-added', 'video-room-04', 'p03', RoomParticipantTrackAdded::class],
            'TrackRemoved' => ['track-removed', 'video-room-05', 'p04', RoomParticipantTrackRemoved::class],
            'TrackEnabled' => ['track-enabled', 'video-room-05', 'p04', RoomParticipantTrackEnabled::class],
            'TrackDisabled' => ['track-disabled', 'video-room-05', 'p04', RoomParticipantTrackDisabled::class],
        ];
    }

    /**
     * @param array<string, string> $body
     */
    private function createRequest(array $body): Request
    {
        return new Request(
            [],
            $body,
            [],
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']
        );
    }
}
