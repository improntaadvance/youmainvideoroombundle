<?php

namespace Tests\Youmain\VideoRoomBundle\Unit\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Twilio\Rest\Client;
use Twilio\Rest\Video;
use Twilio\Rest\Video\V1;
use Twilio\Rest\Video\V1\Room\ParticipantInstance;
use Twilio\Rest\Video\V1\Room\ParticipantList;
use Twilio\Rest\Video\V1\RoomContext;
use Twilio\Rest\Video\V1\RoomInstance;
use Twilio\Rest\Video\V1\RoomList;
use Youmain\VideoRoomBundle\Model\ParticipantInterface;
use Youmain\VideoRoomBundle\Model\TwilioRoomType;
use Youmain\VideoRoomBundle\Model\TwilioVideoRoomInterface;
use Youmain\VideoRoomBundle\Service\TwilioRoomManager;
use Youmain\VideoRoomBundle\Service\VideoRoomNamerInterface;

class TwilioRoomManagerTest extends TestCase
{
    /**
     * @var Client&MockObject
     */
    private $twilioClient;

    /**
     * @var V1&MockObject
     */
    private $twilioV1Mock;

    /**
     * @var RoomList&MockObject
     */
    private $twilioRoomsMock;

    /**
     * @var ParticipantList&MockObject
     */
    private $twilioParticipantList;

    public function setUp(): void
    {
        $twilioParticipantList = $this->createMock(ParticipantList::class);
        $twilioClient = $this->createMock(Client::class);
        $twilioClient->video = $this->createMock(Video::class);
        $roomContext = $this->createMock(RoomContext::class);
        $roomContext->participants = $twilioParticipantList;

        $twilioClient->video->method('__call')->with($this->equalTo('rooms'))->willReturn($roomContext);

        $this->twilioV1Mock = $this->createMock(V1::class);
        $twilioClient->video->v1 = $this->twilioV1Mock;

        $this->twilioRoomsMock = $this->createMock(RoomList::class);
        $twilioClient->video->v1->rooms = $this->twilioRoomsMock;

        $this->twilioParticipantList = $twilioParticipantList;
        $this->twilioClient = $twilioClient;
    }

    /**
     * @test
     */
    public function create_room_success(): void
    {
        $this->twilioRoomsMock->expects($this->once())
            ->method('create')
            ->with($this->equalTo([
                'uniqueName' => 'video-room',
                'type' => 'group',
            ]))
            ->willReturn(new RoomInstance($this->twilioClient->video->v1, ['sid' => 'SID1234']));

        $sid = 'sid';
        $apiKeySid = 'apiKeySid';
        $apiKeySecret = 'apiKeySecret';

        /**
         * @var VideoRoomNamerInterface&MockObject
         */
        $videoRoomNamer = $this->createMock(VideoRoomNamerInterface::class);

        $service = new TwilioRoomManager(
            $this->twilioClient,
            $videoRoomNamer,
            $sid,
            $apiKeySid,
            $apiKeySecret,
            null
        );

        /**
         * @var TwilioVideoRoomInterface&MockObject
         */
        $videoRoom = $this->createMock(TwilioVideoRoomInterface::class);
        $videoRoom->method('getRoomSlug')->willReturn('video-room');
        $videoRoom->method('getMaxSize')->willReturn(4);
        $videoRoom->method('getTwilioRoomType')->willReturn(TwilioRoomType::GROUP);

        $videoRoomNamer->expects($this->once())
            ->method('generate')
            ->with($this->equalTo($videoRoom))
            ->willReturn('video-room');

        $room = $service->createRoom($videoRoom);

        $this->assertEquals('SID1234', $room->getId());
    }

    /**
     * @test
     */
    public function create_room_with_callback_success(): void
    {
        $this->twilioRoomsMock->expects($this->once())
            ->method('create')
            ->with($this->equalTo([
                'uniqueName' => 'video-room',
                'type' => 'group',
                'StatusCallback' => 'https://localhost/api-events',
                'StatusCallbackMethod' => 'POST',
            ]))
            ->willReturn(new RoomInstance($this->twilioClient->video->v1, ['sid' => 'SID1234']));

        /**
         * @var VideoRoomNamerInterface&MockObject
         */
        $videoRoomNamer = $this->createMock(VideoRoomNamerInterface::class);

        $service = new TwilioRoomManager(
            $this->twilioClient,
            $videoRoomNamer,
            'sid',
            'apiKeySid',
            'apiKeySecret',
            'https://localhost/api-events'
        );

        /**
         * @var TwilioVideoRoomInterface&MockObject
         */
        $videoRoom = $this->createMock(TwilioVideoRoomInterface::class);
        $videoRoom->method('getRoomSlug')->willReturn('video-room');
        $videoRoom->method('getMaxSize')->willReturn(4);
        $videoRoom->method('getTwilioRoomType')->willReturn(TwilioRoomType::GROUP);

        $videoRoomNamer->expects($this->once())
            ->method('generate')
            ->with($this->equalTo($videoRoom))
            ->willReturn('video-room');

        $room = $service->createRoom($videoRoom);

        $this->assertEquals('SID1234', $room->getId());
    }

    /**
     * @test
     */
    public function get_participant_list_success(): void
    {
        $this->twilioParticipantList->expects($this->once())
            ->method('read')
            ->with($this->equalTo([
            'status' => 'connected',
        ]))
            ->willReturn([
            new ParticipantInstance($this->twilioV1Mock, [
                'sid' => 'sid',
                'room_sid' => 'SID1234',
                'account_sid' => 'accountSid',
                'status' => 'connected',
                'identity' => 'participantIdentity',
                'date_created' => '2017-07-30T20:00:00Z',
                'date_updated' => '2017-07-30T20:00:00Z',
                'start_time' => '2017-07-30T20:00:00Z',
                'end_time' => '2017-07-30T20:00:00Z',
                'duration' => 1,
                'url' => 'url',
                'links' => [],
            ], 'SID1234'),
        ]);

        /**
         * @var VideoRoomNamerInterface&MockObject
         */
        $videoRoomNamer = $this->createMock(VideoRoomNamerInterface::class);

        $service = new TwilioRoomManager(
            $this->twilioClient,
            $videoRoomNamer,
            'sid',
            'apiKeySid',
            'apiKeySecret',
            'https://localhost/api-events'
        );

        /**
         * @var TwilioVideoRoomInterface&MockObject
         */
        $videoRoom = $this->createMock(TwilioVideoRoomInterface::class);
        $videoRoom->method('getRoomSlug')->willReturn('video-room');
        $videoRoom->method('getMaxSize')->willReturn(4);
        $videoRoom->method('getTwilioRoomType')->willReturn(TwilioRoomType::GROUP);

        $participants = $service->getParticipantList($videoRoom);
        $this->assertInstanceOf(ParticipantInterface::class, $participants[0]);
        $this->assertEquals('participantIdentity', $participants[0]->getIdentity());
    }
}
