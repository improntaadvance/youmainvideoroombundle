<?php

namespace Youmain\VideoRoomBundle\Model;

interface TwilioVideoRoomInterface extends VideoRoomInterface
{
    public function getTwilioRoomType(): TwilioRoomType;
}
