<?php

namespace Youmain\VideoRoomBundle\Model;

interface ParticipantInterface
{
    public function getIdentity(): string;
}
