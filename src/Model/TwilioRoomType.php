<?php

namespace Youmain\VideoRoomBundle\Model;

enum TwilioRoomType:string
{
    case GO = 'go';
    case PEER_TO_PEER = 'peer-to-peer';
    case GROUP = 'group';

    public function toString(): string
    {
        return match ($this) {
            self::GO => 'go',
            self::PEER_TO_PEER => 'peer-to-peer',
            self::GROUP => 'group',
        };
    }
}
