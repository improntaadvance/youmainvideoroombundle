<?php

namespace Youmain\VideoRoomBundle\Model;

class Participant implements ParticipantInterface
{
    public function __construct(
        private string $identity
    ) {
    }

    public function getIdentity(): string
    {
        return $this->identity;
    }
}
