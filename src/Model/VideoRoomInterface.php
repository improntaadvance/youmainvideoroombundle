<?php

namespace Youmain\VideoRoomBundle\Model;

interface VideoRoomInterface
{
    public function getRoomSlug(): string;

    public function getMaxSize(): int;
}
