<?php

namespace Youmain\VideoRoomBundle\Model;

class RoomInstance implements RoomInstanceInteface
{
    public function __construct(
        private string $id
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }
}
