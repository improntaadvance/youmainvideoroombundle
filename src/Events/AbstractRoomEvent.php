<?php

namespace Youmain\VideoRoomBundle\Events;

abstract class AbstractRoomEvent
{
    public function __construct(
        private string $roomId
    ) {
    }

    public function getRoomId(): string
    {
        return $this->roomId;
    }
}
