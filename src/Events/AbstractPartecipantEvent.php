<?php

namespace Youmain\VideoRoomBundle\Events;

abstract class AbstractPartecipantEvent extends AbstractRoomEvent
{
    public function __construct(
        string $roomSlug,
        private string $participantId
    ) {
        parent::__construct($roomSlug);
    }

    public function getParticipantId(): string
    {
        return $this->participantId;
    }
}
