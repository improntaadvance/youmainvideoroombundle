<?php

namespace Youmain\VideoRoomBundle\Service;

use Youmain\VideoRoomBundle\Model\VideoRoomInterface;

class VideoRoomNamer implements VideoRoomNamerInterface
{
    public function generate(VideoRoomInterface $room): string
    {
        return $room->getRoomSlug();
    }

    public function parse(string $videoRoomName): string
    {
        return $videoRoomName;
    }
}
