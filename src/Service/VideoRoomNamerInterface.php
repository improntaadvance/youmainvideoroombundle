<?php

namespace Youmain\VideoRoomBundle\Service;

use Youmain\VideoRoomBundle\Model\VideoRoomInterface;

interface VideoRoomNamerInterface
{
    public function generate(VideoRoomInterface $room): string;

    public function parse(string $videoRoomName): string;
}
