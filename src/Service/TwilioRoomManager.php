<?php

namespace Youmain\VideoRoomBundle\Service;

use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client as TwilioClient;
use Twilio\Rest\Video\V1\Room\ParticipantInstance;
use Youmain\VideoRoomBundle\Model\Participant;
use Youmain\VideoRoomBundle\Model\ParticipantInterface;
use Youmain\VideoRoomBundle\Model\RoomInstance;
use Youmain\VideoRoomBundle\Model\RoomInstanceInteface;
use Youmain\VideoRoomBundle\Model\TwilioVideoRoomInterface;
use Youmain\VideoRoomBundle\Model\VideoRoomInterface;

class TwilioRoomManager
{
    public function __construct(
        private TwilioClient $client,
        private VideoRoomNamerInterface $videoRoomNamer,
        private string $twilioSid,
        private string $twilioApiKeySid,
        private string $twilioApiKeySecret,
        private ?string $callbackUrl
    ) {
    }

    public function createRoom(TwilioVideoRoomInterface $room): RoomInstanceInteface
    {
        $data = [
            'uniqueName' => $this->getVideoRoomUniqueName($room),
            'type' => $room->getTwilioRoomType()->toString(),
        ];

        if (null !== $this->callbackUrl) {
            $data['StatusCallback'] = $this->callbackUrl;
            $data['StatusCallbackMethod'] = 'POST';
        }

        $roomInstance = $this->client->video->v1->rooms->create($data);

        $room = new RoomInstance($roomInstance->sid);

        return $room;
    }

    public function closeRoom(TwilioVideoRoomInterface $room): void
    {
        $uniqueName = $this->getVideoRoomUniqueName($room);
        $this->client->video->v1->rooms($uniqueName)->update('completed');
    }

    public function createParticipantToken(VideoRoomInterface $room, ParticipantInterface $participant): string
    {
        $token = new AccessToken(
            $this->twilioSid,
            $this->twilioApiKeySid,
            $this->twilioApiKeySecret,
            3600,
            $participant->getIdentity()
        );

        $grant = new VideoGrant();

        $grant->setRoom($this->getVideoRoomUniqueName($room));
        $token->addGrant($grant);

        return $token->toJWT();
    }

    public function kickParticipant(VideoRoomInterface $room, ParticipantInterface $participant): void
    {
        $uniqueName = $this->getVideoRoomUniqueName($room);

        $this->client->video->rooms($uniqueName)
            ->participants($participant->getIdentity())
            ->update(['status' => 'disconnected']);
    }

    private function getVideoRoomUniqueName(VideoRoomInterface $room): string
    {
        return $this->videoRoomNamer->generate($room);
    }

    /** @return ParticipantInterface[] */
    public function getParticipantList(VideoRoomInterface $room): array
    {
        $uniqueName = $this->getVideoRoomUniqueName($room);

        $result = $this->client->video->rooms($uniqueName)
            ->participants
            ->read(['status' => 'connected']);

        return array_map(fn (ParticipantInstance $participant) => new Participant($participant->identity), $result);
    }
}
