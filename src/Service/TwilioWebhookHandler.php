<?php

namespace Youmain\VideoRoomBundle\Service;

use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Youmain\VideoRoomBundle\Events\RoomCreated;
use Youmain\VideoRoomBundle\Events\RoomEnded;
use Youmain\VideoRoomBundle\Events\RoomParticipantConnected;
use Youmain\VideoRoomBundle\Events\RoomParticipantDisconnected;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackAdded;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackDisabled;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackEnabled;
use Youmain\VideoRoomBundle\Events\RoomParticipantTrackRemoved;
use Youmain\VideoRoomBundle\Events\RoomRecordingCompleted;
use Youmain\VideoRoomBundle\Events\RoomRecordingFailed;
use Youmain\VideoRoomBundle\Events\RoomRecordingStarted;

class TwilioWebhookHandler
{
    public function __construct(
        private VideoRoomNamerInterface $videoRoomNamer,
        private ?MessageBusInterface $eventBus = null,
        private ?LoggerInterface $logger = null,
    ) {
    }

    public function handle(Request $request): Response
    {
        if (null === $this->eventBus) {
            throw new RuntimeException('The service @twilio_webhook_event_bus is required to use this handler');
        }

        /**
         * @var array<string, string>
         */
        $bodyContent = $request->request->all();

        // Log incoming request to filesystem log
        $incomingRequest = array_merge(
            $bodyContent,
            [
                '_query' => $request->query->all(),
                '_headers' => $request->headers->all(),
            ]
        );

        if (null !== $this->logger) {
            if (false !== $jsonAsString = json_encode($incomingRequest)) {
                $this->logger->info($jsonAsString);
            }
        }

        $this->parseEvent($bodyContent);

        return new Response();
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function parseEvent(array $twilioEvent): void
    {
        if (false === array_key_exists('StatusCallbackEvent', $twilioEvent)) {
            if (null !== $this->logger) {
                $this->logger->error('Unable retrive event for this hook');
            }
            throw new \Exception('Exception in parseEvent');
        }

        $callbackEvent = $twilioEvent['StatusCallbackEvent'];

        switch ($callbackEvent) {
                case 'room-created':
                    $event = $this->roomCreated($twilioEvent);
                    break;
                case 'room-ended':
                    $event = $this->roomEnded($twilioEvent);
                    break;
                case 'participant-connected':
                    $event = $this->roomParticipantConnected($twilioEvent);
                    break;
                case 'participant-disconnected':
                    $event = $this->roomParticipantDisconnected($twilioEvent);
                    break;
                case 'track-added':
                    $event = $this->roomParticipantTrackAdded($twilioEvent);
                    break;
                case 'track-removed':
                    $event = $this->roomParticipantTrackRemoved($twilioEvent);
                    break;
                case 'track-enabled':
                    $event = $this->roomParticipantTrackEnabled($twilioEvent);
                    break;
                case 'track-disabled':
                    $event = $this->roomParticipantTrackDisabled($twilioEvent);
                    break;
                case 'recording-started':
                    $event = $this->roomRecordingStarted($twilioEvent);
                    break;
                case 'recording-completed':
                    $event = $this->roomRecordingCompleted($twilioEvent);
                    break;
                case 'recording-failed':
                    $event = $this->roomRecordingFailed($twilioEvent);
                    break;
                default:
                    if (null !== $this->logger) {
                        $this->logger->warning(sprintf('Unknown event: %s', $callbackEvent));
                    }

                    throw new \Exception('Exception in parseEvent');
            }

        if (null !== $this->eventBus) {
            $this->eventBus->dispatch($event);
        }
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomCreated(array $twilioEvent): RoomCreated
    {
        $this->validateRoomEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);

        return new RoomCreated($roomId);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomEnded(array $twilioEvent): RoomEnded
    {
        $this->validateRoomEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);

        return new RoomEnded($roomId);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomParticipantConnected(array $twilioEvent): RoomParticipantConnected
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);
        $partecipantIdentity = $twilioEvent['ParticipantIdentity'];

        return new RoomParticipantConnected($roomId, $partecipantIdentity);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomParticipantDisconnected(array $twilioEvent): RoomParticipantDisconnected
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);
        $partecipantIdentity = $twilioEvent['ParticipantIdentity'];

        return new RoomParticipantDisconnected($roomId, $partecipantIdentity);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomParticipantTrackAdded(array $twilioEvent): RoomParticipantTrackAdded
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);
        $partecipantIdentity = $twilioEvent['ParticipantIdentity'];

        return new RoomParticipantTrackAdded($roomId, $partecipantIdentity);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomParticipantTrackRemoved(array $twilioEvent): RoomParticipantTrackRemoved
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);
        $partecipantIdentity = $twilioEvent['ParticipantIdentity'];

        return new RoomParticipantTrackRemoved($roomId, $partecipantIdentity);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomParticipantTrackEnabled(array $twilioEvent): RoomParticipantTrackEnabled
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);
        $partecipantIdentity = $twilioEvent['ParticipantIdentity'];

        return new RoomParticipantTrackEnabled($roomId, $partecipantIdentity);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomParticipantTrackDisabled(array $twilioEvent): RoomParticipantTrackDisabled
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);
        $partecipantIdentity = $twilioEvent['ParticipantIdentity'];

        return new RoomParticipantTrackDisabled($roomId, $partecipantIdentity);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomRecordingStarted(array $twilioEvent): RoomRecordingStarted
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);

        return new RoomRecordingStarted($roomId);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomRecordingCompleted(array $twilioEvent): RoomRecordingCompleted
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);

        return new RoomRecordingCompleted($roomId);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function roomRecordingFailed(array $twilioEvent): RoomRecordingFailed
    {
        $this->validatePartecipantEvent($twilioEvent);

        $roomId = $this->extractIdFromUniqueName($twilioEvent['RoomName']);

        return new RoomRecordingFailed($roomId);
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function validateRoomEvent(array $twilioEvent): void
    {
        if (false === array_key_exists('RoomName', $twilioEvent)) {
            if (null !== $this->logger) {
                $this->logger->warning('Unable retrive "room name"');
            }
            throw new \Exception('Exception in room-created');
        }
    }

    /**
     * @param array<string, string> $twilioEvent
     */
    private function validatePartecipantEvent(array $twilioEvent): void
    {
        $this->validateRoomEvent($twilioEvent);

        if (false === array_key_exists('ParticipantIdentity', $twilioEvent)) {
            if (null !== $this->logger) {
                $this->logger->warning('Unable retrive "participant identity"');
            }
            throw new \Exception('Exception in room-created');
        }
    }

    private function extractIdFromUniqueName(string $uniqueName): string
    {
        return $this->videoRoomNamer->parse($uniqueName);
    }
}
