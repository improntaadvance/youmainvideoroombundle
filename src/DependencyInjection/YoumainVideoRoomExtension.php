<?php

namespace Youmain\VideoRoomBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class YoumainVideoRoomExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs, $container);
        if (null === $configuration) {
            throw new Exception('Invalid configuration');
        }

        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('twilio.client');
        $definition->setArgument(0, $config['twilio_sid']);
        $definition->setArgument(1, $config['twilio_token']);

        $definition = $container->getDefinition('youmain.twilio_room_manager');
        $definition->setArgument('$twilioSid', $config['twilio_sid']);
        $definition->setArgument('$twilioApiKeySid', $config['twilio_apikey_sid']);
        $definition->setArgument('$twilioApiKeySecret', $config['twilio_apikey_secret']);
        $definition->setArgument('$callbackUrl', $config['twilio_webhook']);

        $definition = $container->getDefinition('youmain.twilio_webhook_handler');

        if (is_string($config['twilio_webhook_event_bus'])) {
            $definition->setArgument('$eventBus', new Reference($config['twilio_webhook_event_bus']));
        } else {
            $definition->setArgument('$eventBus', null);
        }

        if (is_string($config['twilio_webhook_logger'])) {
            $definition->setArgument('$logger', new Reference($config['twilio_webhook_logger']));
        }
    }

    public function getAlias(): string
    {
        return 'youmain_video_room';
    }
}
