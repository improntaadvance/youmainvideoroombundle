<?php

namespace Youmain\VideoRoomBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('youmain_video_room');
        $rootNode = $treeBuilder->getRootNode();

        $config = $rootNode->children();
        $config->scalarNode('twilio_sid')->isRequired();
        $config->scalarNode('twilio_token')->isRequired();
        $config->scalarNode('twilio_apikey_sid')->isRequired();
        $config->scalarNode('twilio_apikey_secret')->isRequired();
        $config->scalarNode('twilio_webhook')->defaultNull();
        $config->scalarNode('twilio_webhook_logger')->defaultNull();
        $config->scalarNode('twilio_webhook_event_bus')->defaultNull();

        return $treeBuilder;
    }
}
