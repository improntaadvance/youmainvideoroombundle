# Youmain VideoRoomBundle

Modulo per implementare facilmente la gestione delle video room di twilio su progetti Symfony.

## Install


## Configuration

Nel file `config/packages/youmain_video_room.yaml` andare ad inserire le chiavi di twilio:

```yaml
youmain_video_room:
  twilio_sid: ''
  twilio_token: ''
  twilio_apikey_sid: ''
  twilio_apikey_secret: ''
```

## Usage

Ci sono delle interfacce da implementare che definiscono:

- `Youmain\VideoRoomBundle\Model\TwilioVideoRoomInterface`: una room con la sua configurazione
- `Youmain\VideoRoomBundle\Model\ParticipantInterface`: un partecipante

```php
use Youmain\VideoRoomBundle\Service\TwilioRoomManager;

// importarsi il servizio
public function __construct(
    private TwilioRoomManager $twilioRoomManager,
) {}

// usarlo
$this->twilioRoomManager->createRoom($room);
```

## Webhook
Twilio comunica diversi stati tramite webhook. Per attivarlo, bisogna creare una rotta a proprio piacimento:

```php
<?php
use Youmain\VideoRoomBundle\Service\TwilioWebhookHandler;

class TwilioWebhookApi
{
  public function __construct(
    private TwilioWebhookHandler $webhookHandler
  ) {}

  #[Route('/api/twilio-webhook')]
  public function __invoke(Request $request): Response
  {
    return $this->webhookHandler->handle($request);
  }
}
```

Dopodiché va configurato nel file `config/packages/youmain_video_room.yaml`:

```yaml
youmain_video_room:
  twilio_webhook: https://dominio.it/api/twilio-webhook
```


### Logs
Per scrivere su file tutti i log relativi al webhook bisogna per prima cosa generare un canale monolog:

```yaml
monolog:
  channels: [twilio]
  handlers:
      twilio_log:
          type: stream
          path: "%kernel.logs_dir%/twilio-log-%kernel.environment%.log"
          level: info
          channels: [twilio]
          formatter: 'monolog.formatter.json'
```

Dopodiché va configurato nel file `config/packages/youmain_video_room.yaml` con l'id del servizio di logger da usare:

```yaml
youmain_video_room:
  twilio_webhook_logger: 'monolog.logger.twilio'
```
